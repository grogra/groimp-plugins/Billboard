package de.grogra.billboard;

import java.io.File;
import java.io.IOException;

import de.grogra.util.MimeType;

/**
 * This is a simple implementation of the abstract class {@link #Billboarder}.
 * By invoke {@link #billboarding()} only rendered images will be write to disk.
 * Example:
 * <code>
 * File file			= new File("C:\\Test\\");
 * BasicBillboarder bb 	= new BasicBillboarder(file, Billboarder.MT_PNG);
 * 	
 * bb.initialize(5, 0, 20);
 * bb.billboarding();
 * </code>
 * @author adgen
 *
 */
public class BasicBillboarder extends Billboarder {
		
	public BasicBillboarder(File file, MimeType mt)
	{
		super.camera 	= new RotationCamera();
		super.view3d	= camera.getView3D();
		
		try {
			super.setDestination(file, mt);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void setRunLater()
	{
		// Rotate the camera to the next viewpoint
		nextSide();
										
		// Invoke next render-thread
		billboarding();				
	}
}