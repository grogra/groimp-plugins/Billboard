package de.grogra.billboard;

import java.io.File;
import java.io.IOException;

import javax.vecmath.Point3d;

import de.grogra.graph.Attributes;
import de.grogra.graph.Graph;
import de.grogra.graph.GraphState;
import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.imp3d.VolumeAttribute;
import de.grogra.imp3d.objects.Null;
import de.grogra.imp3d.objects.Parallelogram;
import de.grogra.imp3d.objects.ShadedNull;
import de.grogra.imp3d.shading.Light;
import de.grogra.math.TVector3d;
import de.grogra.util.MimeType;
import de.grogra.vecmath.geom.HalfSpace;
import de.grogra.vecmath.geom.SimpleUnion;
import de.grogra.vecmath.geom.Variables;
import de.grogra.xl.util.ObjectList;

/**
 * This Billboarder render the object slice by slice from the back to the front.
 * After that the camera will rotate 90� to the left and render slice by slice again.
 * And so on, until four sides are rendered. From the top of view, it seems like a
 * grid.
 * 
 * @author adgen
 *
 */

public class GridBillboarder extends Billboarder {

	private int visibleLayer 		= 0;
	private int invisibleLayer  	= 15;
	
	// This will be defined by a given parameter at the method 
	// initialize() and defined how many slice per view has to 
	// be rendered.
	protected int steps				= 0;

	// Counter for steps: defined the actual slice for a view
	private int stepCount			= 0;
	
	// Defined which size a slice has. Also given by parameter
	private double stepWidthX		= 0;
	private double stepWidthY		= 0;
	
	private int oldSides			= 0;
	
	public Point3d min, max;
	
	private double xWidth, yWidth;
		
	//private float boxWidth;

	
	public GridBillboarder(File file, MimeType mt)
	{		
		super.camera 				= new RotationCamera();	
		super.view3d				= camera.getView3D();
		
		try 
		{				
			super.setDestination(file, mt);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public void beginAt(int side, int step)
	{
		currentSide = side;
		stepCount = step-1;
		
		makeNames(currentSide, stepCount);
		
		nextSide(side-1);
		
		griding();
	}
	
	/**
	 * Sets the camera on initial position. This billboarder creates picture only from two
	 * or 4 sides. The parameter <code>steps</code> defined the number of slices has to be
	 * create at every side of view. To reach an optimum coverage of all parts of the model
	 * only odd numbers of slices will be generated. If an even number is given, this will be
	 * reduced by one so the number is odd.
	 * 
	 * @param distance Distance between camera and the origin of the coordinates-system
	 * @param zOfsset The z-axis shift of the camera
	 * @param fourSides If this is true all 4 sides of the model will be rendered by this. Otherwise
	 * 					only the front and left-side will be rendered.
	 * @param steps Odd number of slices for every side of view.
	 */
	public void initialize(double distance, double zOfsset, boolean fourSides, int steps)
	{
		getExtent();
				
		this.steps 		= steps%2 == 0 ? steps-1 : steps;
		stepWidthX		= xWidth/this.steps;
		stepWidthY		= yWidth/this.steps;
		oldSides		= fourSides ? 4 : 2;
		
		countZero		= (int) Math.floor(Math.log10(steps));
		
		super.initialize(distance, zOfsset, fourSides ? 4 : 2);
		super.sides++;
		
		griding();
	}
	
	/**
	 * Defined which layer is for the visible and which for the invisible
	 * objects. The default-value for visible is 0 and invisible 15. 
	 * 
	 * @param visible The Number of the visible layer in the 3D-view
	 * @param invisible The Number of the invisible layer in the 3D-view
	 */
	public void setLayer(int visible, int invisible)
	{
		if (visible < Attributes.LAYER_COUNT && visible >= 0)
			visibleLayer = visible;
		
		if (invisible < Attributes.LAYER_COUNT && invisible >= 0)
			invisibleLayer = invisible;
	}
	
	public void griding(int step)
	{
		this.stepCount = step%steps;
		griding();
	}
	
	private void griding()
	{		
		final HalfSpace hs1	= new HalfSpace();
		final HalfSpace hs2	= new HalfSpace();
		
		double pos1			= 0.0;
		double pos2			= 0.0;
		
		Point3d origin1		= new Point3d(0.0, 0.0, 0.0);
		Point3d origin2		= new Point3d(0.0, 0.0, 0.0);
		TVector3d axis		= new TVector3d(0, 0, 0);
		
		int sideOfView		= currentSide == 0 ? 0 : currentSide-1;
		
		switch (sideOfView)
		{
			case 0 :		// Front
				pos1		= max.y - ((++stepCount) * stepWidthY);
				pos2		= pos1 + stepWidthY;
				origin1 	= new Point3d(0.0, pos1, 0.0);
				origin2 	= new Point3d(0.0, pos2, 0.0);
				axis 		= new TVector3d(0, -1, 0);
				break;
			case 1 :		// Left
				pos1		= max.x - ((++stepCount) * stepWidthX);
				pos2		= pos1 + stepWidthX;
				origin1 	= new Point3d(pos1, 0.0, 0.0);
				origin2 	= new Point3d(pos2, 0.0, 0.0);
				axis 		= new TVector3d(-1, 0, 0);
				break;
			case 2 :		// Back
				pos1		= min.y + ((++stepCount) * stepWidthY);
				pos2		= pos1 - stepWidthY;
				origin1 	= new Point3d(0.0,pos1,0.0);
				origin2 	= new Point3d(0.0,pos2,0.0);
				axis 		= new TVector3d(0, 1, 0);
				break;
			case 3 :		// Right
				pos1		= min.x + ((++stepCount) * stepWidthX);
				pos2		= pos1 - stepWidthX;
				origin1 	= new Point3d(pos1, 0.0, 0.0);
				origin2 	= new Point3d(pos2, 0.0, 0.0);
				axis 		= new TVector3d(1, 0, 0);
				break;
		}
				
		hs1.setTransformation(origin1, axis);
		hs2.setTransformation(origin2, axis);
		
		ObjectList<Node> stack = new ObjectList<Node> (100);
		stack.push (graph().getRoot());
		
		Point3d location = new Point3d();
		
		while (!stack.isEmpty ())
		{
			// get next node
			Node n = (Node) stack.pop ();
			if((n instanceof ShadedNull) && (!(n instanceof Light) || (n instanceof Parallelogram)))
			{
				location = location(((Null) n));
				// An object is visible if it lies inside the first HalfSpace but not in the second.
				if( hs1.contains(location, true)  && !hs2.contains(location, true))
				{
					((Null) n).setLayer(visibleLayer);
				} else {
					((Null) n).setLayer(invisibleLayer);
				}
			}
			// visit all edges e starting with n
			for (Edge e = n.getFirstEdge (); e != null; e = e.getNext (n))
			{
				// Get target node of e
				Node t = e.getTarget ();
				// pr�fen, ob Kante vorw�rts durchlaufen wird und
				// Verzweigung oder Nachfolger angibt
				if ((t != n) && e.testEdgeBits (Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE))
				{
					// neuen Nachfahren gefunden, auf Stack schieben
					stack.push (t);
				}
			}
		}
	}
	
	private void getExtent()
	{
		ObjectList<Node> stack = new ObjectList<Node> (100);
		stack.push (graph().getRoot());
		SimpleUnion cv = new SimpleUnion(){};
				
		min = new Point3d();
		max = new Point3d();
		
		while (!stack.isEmpty ())
		{
			// get next node
			Node n = (Node) stack.pop ();
			if((n instanceof ShadedNull) && (!(n instanceof Light) || (n instanceof Parallelogram)))
			{
				cv.volumes.add( VolumeAttribute.getVolume (n, true, GraphState.current (graph ())));
			}
			// visit all edges e starting with n
			for (Edge e = n.getFirstEdge (); e != null; e = e.getNext (n))
			{
				// Get target node of e
				Node t = e.getTarget ();
				// pr�fen, ob Kante vorw�rts durchlaufen wird und
				// Verzweigung oder Nachfolger angibt
				if ((t != n) && e.testEdgeBits (Graph.BRANCH_EDGE | Graph.SUCCESSOR_EDGE))
				{
					// neuen Nachfahren gefunden, auf Stack schieben
					stack.push (t);
				}
			}
		}
		
		cv.getExtent(min, max, new Variables());
		
		xWidth	= Math.abs(max.x - min.x);
		yWidth	= Math.abs(max.y - min.y);
	}
	
	protected void makeNames(int stopSide, int stopStep)
	{
		String appendfix, tempFileName;
		
		for(int i=1;i<=stopSide;i++)
		{
			int tempStop = i < stopSide ? steps : stopStep;
			for(int e=1;e<=tempStop;e++)
			{
				appendfix 	= "_" + getLeadingZeros(steps, e) + e;	
				
				// Filename of the rendered billboard
				tempFileName = prefix + i + appendfix +  "." + suffix;
				
				// Remember the path and filename for further actions
				insertName((imageFolder != "" ?  imageFolder + "/" : "") + tempFileName);
			}
		}
	}
	
	public void makeAllNames()
	{
		makeNames(oldSides, steps);
	}
	
	public void billboarding()
	{				
		super.appendfix = "_" + getLeadingZeros(steps, stepCount) + stepCount;
	
		super.billboarding();
		
		if(currentSide > 0)
			currentSide--;
	}
	
	@Override
	protected void setRunLater()
	{		
		if((stepCount+1) == steps && currentSide == oldSides)
			sides--;
		
		// If all slices are rendered go to next side or finish
		if(!(this.stepCount < this.steps))
		{
			// Finish this method if all sides and slices are rendered
			if(!(currentSide < oldSides))			
				return;			

			// Otherwise go to the next side
			nextSide();
			currentSide++;
			
			// Start with first slice 
			this.stepCount = 0;	
			
		}
		// Make actual slice invisible and the next visible
		griding();						
		// Invoke next render-thread
		billboarding();
	}	
}