package de.grogra.billboard.vrml;

public interface VRMLBuilder {
	
	public void writeVRML(int width, int height);
	
}
