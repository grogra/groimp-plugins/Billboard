package de.grogra.billboard.vrml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import de.grogra.billboard.BBResources;
import de.grogra.billboard.BasicBillboarder;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.MimeType;

public class VRMLDynamicBuilder extends BasicBillboarder implements VRMLBuilder{

	protected String vrmlFileName	= "bb";

	private String vrmlTempl		= "";
	
	protected File vrmlDest;
	
	public VRMLDynamicBuilder(File file, MimeType mt)
	{
		super (file, mt);		
		
		vrmlDest = new File(file.getPath() + 
								File.separator + 
								vrmlFileName + ".wrl"
							);
		
		imageFolder = "";
		
		vrmlTempl				=	BBResources.getVRMLTempl(this.getClass(), "vrmldyntempl.txt");
	}
	
	public VRMLDynamicBuilder(File file, MimeType mt, String subDirectionary) throws IOException
	{
		super (file, mt);		
		
		vrmlDest				= new File(file.getPath() + 
										File.separator + 
										vrmlFileName + ".wrl");

		file					= new File(file.getPath() + 
										File.separator + 
										subDirectionary);
		if(!file.isDirectory())
		{
			file.mkdir();
		}
		
		imageFolder 			= subDirectionary;
		
		vrmlTempl				=	BBResources.getVRMLTempl(this.getClass(), "vrmldyntempl.txt");
	}

	public void writeVRML(int width, int height) 
	{
		String vrmlOutput	= vrmlTempl;
		String vrmlProtos 	= "";		
				
		int i = 0;
		
		double rel = ((double) height) / ((double) width);
		
		while(i<sides)
			vrmlProtos += "PngFrame { url \"" + getBBName(i++) + "\" } " + System.getProperty("line.separator") + "\t\t";	
			
		vrmlOutput	=	vrmlOutput.replaceAll("::REL_HEIGHT::", Double.toString(rel));
		vrmlOutput	=	vrmlOutput.replaceAll("::PNG_PROTO_CALL::", vrmlProtos);
		vrmlOutput	=	vrmlOutput.replaceAll("::BILL_NUM::", Integer.toString(sides));
		
		try 
		{
			FileWriter outputStream 	= new FileWriter(vrmlDest);
			BufferedWriter  outputObj	= new BufferedWriter(outputStream);
		
			outputObj.write(vrmlOutput);

			outputObj.flush();
			outputStream.close();
			
		} catch (IOException ex)
		{
			Workbench.current().logGUIInfo ("Error: " + ex.getMessage());		
		}		
	}
	
	@Override
	protected void finalAction()
	{
		// Write the VRML-file
		writeVRML(imgWidth, imgHeight);
		
		//super.finalAction();
	}
}
