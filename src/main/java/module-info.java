module billboard {
	exports de.grogra.billboard;
	exports de.grogra.billboard.vrml;
	
	requires graph;
	requires imp;
	requires imp3d;
	requires math;
	requires platform;
	requires platform.core;
	requires raytracer;
	requires utilities;
	requires vecmath;
	requires xl.core;
	requires java.datatransfer;
	requires java.desktop;
	
	opens de.grogra.billboard to utilities;
}