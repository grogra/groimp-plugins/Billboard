## Billboard Plugin

Support for Billboard object in GroIMP. Billboard can be added to the scene and display an image. It works as follow: <br> File file = new File ('/path/to/an/image/CucLeaf.png'); <br> BasicBillboarder bb = new BasicBillboarder(file, Billboarder.MT_PNG)
